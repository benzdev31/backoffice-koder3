<?php

namespace PHPMaker2021\testslimvuejsfiresstore;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Customer controller
 */
class CustomerController extends ControllerBase
{

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        return $this->runPage($request, $response, $args, "Customer");
    }
}
