<?php

namespace PHPMaker2021\testslimvuejsfiresstore;

use Google\Cloud\Firestore\FirestoreClient;
// Page object
$Customer = &$Page;
// Base path
$basePath = BasePath(true);
require_once(realpath(dirname(__FILE__) . '/../src/Firestore.php'));
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i data-phrase="HomePage" class="fas fa-database ew-icon"
                                          data-caption="database"></i>
                    Customer</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อโครงการ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $firestore = new Firestore('customer');
                    $db_customers = $firestore->getDocumentAll('crate_date');
                    foreach ($db_customers as $key => $item) {
                        ?>
                        <tr>
                            <td><?= $key + 1 ?></td>
                            <td><?= $item["id"] ?></td>
                            <td>
                                <button name="action"
                                        class="btn btn-info btn-xs"
                                        onclick="viewModelDetail('<?= $item["id"] ?>')"
                                <span class="btn-label">
                                        <i class="fas fa-eye ew-icon"></i>
                                        </span>
                                ดูข้อมูล
                                </button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อโครงการ</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="viewRowModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
														<span class="fw-mediumbold">
														ดูข้อมูล</span>
                    <span class="fw-light">
															Customer
														</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>customer_code</label>
                                <select class="custom-select form-control-border"
                                        onchange="viewContentField(this)"
                                        id="selectListField">
                                </select>
                            </div>
                            <hr>
                            <div id="contentEdit" style="display: none;">
                                <div class="form-group" align="center">
                                    <input id="field_name" name="field_name" type="hidden" class="form-control" value="" disabled>
                                    <input id="field_value" name="field_value" type="text" class="form-control" value="" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer no-bd">
                <div id="buttonSave"  style="display: none;">
                    <button  type="button" class="btn btn-outline-success" onclick="saveCollectionDB()">บันทึกข้อมูล</button>
                </div>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?= GetDebugMessage() ?>
<script type="text/javascript">
    loadjs.ready("load", function () {
        $(document).ready(function () {
            $(function () {
                $("#example1").DataTable({
                    "responsive": true,
                    "lengthChange": false,
                    "autoWidth": false,
                }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                });
            });
        });



    });

    let dataList = [];
    let keyDataId = '';
    let indexArr = null;
    // View popup Model db firestore
    function viewModelDetail(id)
    {
        const myElement = document.getElementById('contentEdit');
        if (myElement) myElement.style.display = "none";
        const myElement2 = document.getElementById('buttonSave');
        if (myElement2) myElement2.style.display = "none";

        $.ajax({
            dataType: 'json',
            url: '/api/collection/customer/'+id,
        }).done(function(data){
            console.log(data.data);
            var options = [];
            options += '<option selected disabled>กรุณาเลือก</option>';
            for (var i = 0; i < data.data.length; i++) {
                options += '<option value="' +i+ '">' + data.data[i].name + '</option>';
                let my_object = {};
                my_object.name = data.data[i].name;
                my_object.value = data.data[i].value;
                dataList.push(my_object);
            }
            $("#selectListField").html(options);
            keyDataId = id;
        });
        $('#viewRowModal').modal('show');
    }
    // View Field value db firestore
    function viewContentField(object)
    {
        const index = object.value;
        // console.log(index);
        // console.log(dataList[index]);
        $("#field_name").val(dataList[index].name);
        $("#field_value").val(dataList[index].value);
        indexArr = index;
        document.getElementById('field_value').disabled = false;
        const myElement = document.getElementById('contentEdit');
        if (myElement) myElement.style.display = "block";
        const myElement2 = document.getElementById('buttonSave');
        if (myElement2) myElement2.style.display = "block";
    }
    // save or update collection db firestore
    function saveCollectionDB()
    {
        const field_name = $("#field_name").val();
        const field_value = $("#field_value").val()
        if (field_value) {
            dataList[indexArr].value = field_value;
            $.ajax({
                url: '/api/collection/customer/update',
                type: 'POST',
                data: {
                    'data': dataList,
                    'field_id': keyDataId,
                    'field_name': field_name,
                    'field_value': field_value,
                },
                success: function(response){
                    console.log(response)
                }
            });
            closeModelDetail();
        } else {
            alert("ERROR")
        }

    }
    // close model and clear js, data
    function closeModelDetail()
    {
        var options = '';
        options += '<option selected disabled>กรุณาเลือก</option>';
        $("#selectListField").html(options);
        const myElement = document.getElementById('contentEdit');
        if (myElement) myElement.style.display = "none";
        const myElement2 = document.getElementById('buttonSave');
        if (myElement2) myElement2.style.display = "none";
        dataList = [];
        keyDataId = '';
        $("#field_name").val("");
        $("#field_value").val("")
        $('#viewRowModal').modal('hide');
    }
</script>