<?php

namespace PHPMaker2021\testslimvuejsfiresstore;

// Menu Language
if ($Language && function_exists(PROJECT_NAMESPACE . "Config") && $Language->LanguageFolder == Config("LANGUAGE_FOLDER")) {
    $MenuRelativePath = "";
    $MenuLanguage = &$Language;
} else { // Compat reports
    $LANGUAGE_FOLDER = "../lang/";
    $MenuRelativePath = "../";
    $MenuLanguage = Container("language");
}

// Navbar menu
$topMenu = new Menu("navbar", true, true);
$topMenu->addMenuItem(3, "mi_Customer", $MenuLanguage->MenuPhrase("3", "MenuText"), $MenuRelativePath . "Customer", -1, "", IsLoggedIn() || AllowListMenu('{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}Customer.php'), false, false, "", "", true);
$topMenu->addMenuItem(2, "mi_users", $MenuLanguage->MenuPhrase("2", "MenuText"), $MenuRelativePath . "UsersList", -1, "", IsLoggedIn() || AllowListMenu('{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}users'), false, false, "", "", true);
echo $topMenu->toScript();

// Sidebar menu
$sideMenu = new Menu("menu", true, false);
$sideMenu->addMenuItem(3, "mi_Customer", $MenuLanguage->MenuPhrase("3", "MenuText"), $MenuRelativePath . "Customer", -1, "", IsLoggedIn() || AllowListMenu('{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}Customer.php'), false, false, "", "", true);
$sideMenu->addMenuItem(2, "mi_users", $MenuLanguage->MenuPhrase("2", "MenuText"), $MenuRelativePath . "UsersList", -1, "", IsLoggedIn() || AllowListMenu('{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}users'), false, false, "", "", true);
echo $sideMenu->toScript();
