<?php
/**
 * PHPMaker 2021 user level settings
 */
namespace PHPMaker2021\testslimvuejsfiresstore;

// User level info
$USER_LEVELS = [["-2","Anonymous"]];
// User level priv info
$USER_LEVEL_PRIVS = [["{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}_users_old_20210325","-2","0"],
    ["{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}users","-2","0"],
    ["{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}Customer.php","-2","0"]];
// User level table info
$USER_LEVEL_TABLES = [["_users_old_20210325","_users_old_20210325"," users old 20210325",true,"{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}",""],
    ["users","users","users",true,"{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}","UsersList"],
    ["Customer.php","Customer","Customer",true,"{900CB8D4-B1CB-4EE6-8256-C68FD3DEBFC6}","Customer"]];
